class ApplicationController < ActionController::API
  before_filter :prepare_params

  private
  def prepare_params
    # Возьмем хеш с params и выкинем из него значения с ключами, которые нам не нужны
    restricted_keys = ["callback", "_", "controller", "action", "period_from", "period_to"]
    @db_params = params.to_hash
    @db_params.delete_if { |key, value| restricted_keys.include? key }
    # Если пользователь указаль несколько IP-адресов, значит,
    # их нужно преобразовать должным образом
    @db_params["source_ip"] = @db_params["source_ip"].split if @db_params.has_key? "source_ip"
    # Если параметр source_ip пустой, значит, его нужно удалить, чтобы не мешался
    @db_params.delete "source_ip" if @db_params["source_ip"].empty? unless @db_params["source_ip"].nil?
  end
end
