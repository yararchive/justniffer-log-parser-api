class JustnifferLogRecordsController < ApplicationController
  def show
    @justniffer_log_records = JustnifferLogRecord.where(@db_params).where("request_timestamp >= ?", params["period_from"]).where("request_timestamp <= ?", params["period_to"])
    # Вывод, адаптированный для Jquery.ajax. Чтобы выводить результат в формате,
    # подходящем для Backbone, раскомментируй следующую строчку
    # render json: @justniffer_log_records
    render text: "#{params['callback']}(#{@justniffer_log_records.to_json})"
  end
end
