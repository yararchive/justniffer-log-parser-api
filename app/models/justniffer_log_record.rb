class JustnifferLogRecord < ActiveRecord::Base
  attr_accessible :close_time, :close_timestamp, :close_timestamp2, :connection, \
  :connection_time, :connection_timestamp, :connection_timestamp2, :idle_time_0, \
  :idle_time_1, :response_time_begin, :response_time_end, :dest_ip, :dest_port, \
  :source_ip, :source_port, :request, :request_timestamp, :request_timestamp2, \
  :request_size, :request_line, :request_method, :request_url, :request_protocol, \
  :request_header, :request_header_host, :request_header_user_agent, \
  :request_header_accept, :request_header_authorization, \
  :request_header_connection, :request_header_content_encoding, \
  :request_header_content_length, :request_header_content_md5, \
  :request_header_cookie, :request_header_range, :request_header_referer, \
  :request_header_keep_alive, :request_header_transfer_encoding, \
  :request_header_via, :response, :response_timestamp, :response_timestamp2, \
  :response_size, :response_time, :response_line, :response_protocol, \
  :response_code, :response_message, :response_header, :response_header_server, \
  :response_header_date, :response_header_content_length, \
  :response_header_content_type, :response_header_content_md5, \
  :response_header_content_encoding, :response_header_content_language, \
  :response_header_transfer_encoding, :response_header_etag, \
  :response_header_cache_control, :response_header_last_modified, \
  :response_header_pragma, :response_header_age, :response_header_connection, \
  :response_header_keep_alive, :response_header_via, :response_header_vary, \
  :response_header_accept_ranges, :response_header_set_cookie
end
