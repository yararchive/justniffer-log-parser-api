JustnifferLogParser
=============

[Andrey Zvorygin](http://vk.com/id828789), 2012

email: umount.dev.brain@ya.ru

___JustnifferLogParser API___ позволяет получить и упорядочить информацию, записанную [JustnifferLogParser](https://github.com/yararchive/justniffer-log-parser) в базу данных.

Система построена на основе [rails-api](https://github.com/rails-api/rails-api). Результаты поиска возвращаются в формате json. Ввиду отсутствия необходимости построения сложной системы приложение состоит всего из одного метода, куда через get параметры передаются значения, которые нужно отфильтровать. Список параметров для фильтрации доступен в описании [JustnifferLogParser](https://github.com/yararchive/justniffer-log-parser/blob/master/README.md#%D0%92%D0%B0%D1%80%D0%B8%D0%B0%D0%BD%D1%82%D1%8B-%D0%B7%D0%BD%D0%B0%D1%87%D0%B5%D0%BD%D0%B8%D0%B9-%D0%BA%D0%BE%D1%82%D0%BE%D1%80%D1%8B%D0%B5-%D0%BC%D0%BE%D0%B6%D0%BD%D0%BE-%D0%BF%D0%B5%D1%80%D0%B5%D0%B4%D0%B0%D1%82%D1%8C-%D0%B2-%D0%BF%D0%B0%D1%80%D0%B0%D0%BC%D0%B5%D1%82%D1%80%D0%B5---logfile-format)